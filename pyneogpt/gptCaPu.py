from transformers import pipeline

generator = pipeline('text-generation', model='EleutherAI/gpt-neo/2.7B')

prompt = "What is love?"

res = generator(prompt, max_lengh=200, do_sample=True, temperature=0.9)

print(res[0]['generated_text'])
