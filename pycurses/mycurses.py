import curses
import time
from curses import wrapper
from curses.textpad import Textbox, rectangle

def main(stdscr):
    curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_YELLOW)
    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    BLUE_AND_YELLOW = curses.color_pair(1)
    GREEN_AND_BLACK = curses.color_pair(2)
    RED_AND_WHITE = curses.color_pair(3)

#    pad = curses.newpad(100,100)
#    stdscr.refresh()
#    #counter_win = curses.newwin(1,10,5,5)
#    for i in range(100):
#       for j in range(26):
#            char = chr(67 + j)
#            pad.addstr(char)
#    for i in range(50):
#        stdscr.clear()
#        stdscr.refresh()
#        pad.refresh(0, i, 5, i, 10, 25 + i)
#        time.sleep(0.2)

    win = curses.newwin(3, 18, 2, 2)
    box = Textbox(win)
    rectangle(stdscr, 1, 1, 5, 20)
    stdscr.refresh()
    box.edit()

    text = box.gather().strip().replace("\n", "")

    stdscr.addstr(10, 40, text)

    stdscr.getch()

wrapper(main)
